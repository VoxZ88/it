/*Задание 4: сделать функцию для вывода модуля числа
Задание 2:Деление двух чисел с остатком
Задание 1:Сделать функцию,выводящую произведение и сумму арифметической прогресси
@link https://gitlab.com/VoxZ88/it
@link http://moria.1586.su/moodle/mod/page/view.php?id=1705multiplay
@author Elizaveta
Доп.Задание 4,2,1
*/
#include <stdlib.h>
#include <iostream>
using namespace std;
/*
@param double a,double b
@return a+b
@example возвращает сумму чисел a и b
*/
double sum(double a,double b)
{
	return a + b;
}
/*
@param double a,double b
@return возвращает разность чисел a и b
@example a - b
*/
double min(double a, double b)
{
	return a - b;
}
/*
@param double a,double b
@return a*b
@example возвращает произведение чисел a на b
*/
double multiplay(double a, double b)
{
	return a * b;
}
/*
@param double a,double b
@return 0,a/b
@example возвращает деление числа a на число b
@throws err = -1
*/
double div(double a, double b, int* err)
{
	if (0 == b)
	{
		*err = -1;
		return 0;
	}
	else return a / b;
}
/*
@param double a
@return a
@example возвращает модуль числа a
*/
double modul(double a)
{
	return a < 0 ? -a : a;
}
/*
@param double a,double b
@return 0, a % b
@example возвращает деление с остатском
*/
double ost(double a, double b, int* err)
{
	if (0 == b) {
		*err = -1;
		return 0;
	}
	else return int(a) % int(b);
}
/*
@param double a,double b
@return res_sum,res_multu
@example возвращает сумму и произведение арифметической прогресси от a0 до N
*/
double arif_chain(double a,double b)
{
	int n;
	cout << " Enter N " << endl;
	cin >> n;
	int res_sum,res_multu;
  for(int i = a; i < n; a + b) {
	 res_sum += a;
	 res_multu *= a;
 	return res_sum,res_multu;
 }
}
/*
@return 0
@example главная функция
@uses iostream
*/
int main()
{
	int* err = 0;
	double f_num, s_num;
	char z;
	double res;
	while (true) 
	{
	 cout << "Command" << endl;
	 cin >> f_num;
	 cin >> z;
	 cin >> s_num;
		switch (z) 
		{
		case '+': res = sum(f_num, s_num);
		break;
		case '-': res = min(f_num, s_num);
		break;
		case '*': res = multiplay(f_num, s_num);
		break;	
        case '/': res = div(f_num, s_num, err);
		break;
        case '%': res = ost(f_num, s_num, err);
	 	break;
	    case 'M': res = modul(f_num);
		break;
	    case 'A': res = arif_chain(f_num, s_num);
		break;
	    default:
		return 0;
		break;
	    }
	if('E' == z) exit(true);
	cout << res << endl;
    }
	return 0;

}
